<?php

namespace Database\Seeders;

use App\Models\User;

use faker\factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = faker::create('pt_BR');


        User::create([
            'nome' => 'Rener Augusto',
            'email' => 'antetegmon@outloo.com',
            'password' => bcrypt('12345678'),
            'role' => 'editor',
            'created_at' => now(),
        ]);

        foreach (range(1, 20) as $index) {
            User::create([
                'nome' => $faker->name,
                'email' => $faker->unique()->saFeEmail,
                'password' => bcrypt('12345678'),
                'role' => 'editor',
                'created_at' => now(),
            ]);
        }
    }
}
