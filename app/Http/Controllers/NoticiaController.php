<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class NoticiaController extends Controller
{
    public function index()
    {
        return view('noticias.index');
    }

    public function categoria()
    {
        return view('noticia.categoria');
    }

    public function login()
    {
        return view('layouts.vizualiar');
    }
}
