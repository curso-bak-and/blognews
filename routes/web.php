<?php

use App\Http\Controllers\site;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SiteControllers::class, 'home']);
Route::get('/buscar', [SiteControllers::class, 'buscar']);
Route::get('/noticias', [NoticiasController::class, 'index']);
Route::get('/noticias/visualizar', [NoticiasController::class, 'visualizar']);
Route::get('/noticias/categoria', [NoticiasController::class, 'categoria']);
Route::get('/login/login', [NoticiasController::class, 'login']);

Route::get('/', function () {
    return view('site.home');
});

Route::get('/noticias', function () {
    return view('noticias.index');
});

Route::get('/noticias/visualizar', function () {
    return view('noticias.visualizar');
});


Route::get('/buscar', function () {
    return view('site.buscar');
});

Route::get('/noticias/categoria', function () {
    return view('home.index');
});

Route::get('/noticias/categoria', function () {
    return view('noticia.categoria');
});

Route::get('/login', function () {
    return view('login.login');
});
